﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CRM_ImapX.Services;


namespace CRM_ImapX.Services.DTO
{
    public class GetEmailsDTO
    {
        public string attachmentName { get; set; }
        public string attachmentDate { get; set; }
    
    }
}