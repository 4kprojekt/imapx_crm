﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace CRM_ImapX.Services
{
    public class XmlGenerateMessage
    {

        public string MessageID { get; set; }
        public string MessageGuid { get; set; }

        [XmlElement(IsNullable = true)]
        public string InReplayTo  { get; set; }
        public string From { get; set; }
        public string Date { get; set; }        
        public string To { get; set; }

        [XmlElement(IsNullable = true)]
        public string Cc { get; set; }

        [XmlElement(IsNullable = true)]
        public string Bcc { get; set; }
      
        [XmlElement(IsNullable = true)]
        public string Subject { get; set; }

        [XmlElement(IsNullable = true)]
        public string Content { get; set; }

        [XmlElement(IsNullable = true)]
        public string Html { get; set; }
         
    }

}