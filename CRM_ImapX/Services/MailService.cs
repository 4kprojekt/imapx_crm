﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using ImapX;
using CRM_ImapX.Services.DTO;
using System.IO;
using System.Xml;
using ImapX.Constants;
using ImapX.Collections;
using ImapX.Enums;
using CRM_ImapX.Models.DB;
using System.Collections.ObjectModel;
using System.Data.Entity;
using CRM_ImapX.Services;
using System.Xml.Serialization;
using CRM_ImapX.Models;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;

namespace CRM_ImapX.Services
{
    public class MailService
    {
        //public static List<GetEmailsDTO> mails = new List<GetEmailsDTO>();

        /// <summary>
        /// Metoda odpowiedzialna za połączenie z pocztą, pobranie wiadomości i zapis załączników
        /// </summary>
        /// <returns></returns>
        public void ReciveUnseenImap()
        {
            string dateFolder = null;
            string messageFolder = null;

            //Połączenie z pocztą

            ImapClient imap = new ImapClient(Properties.Settings.Default.Server, true, false);
            try
            {
            imap.Connect();
            imap.Login(Properties.Settings.Default.Login, Properties.Settings.Default.Password);

            }
            catch (Exception e)
            {

                SenderProcessService.CatchError(e);
            }
    
            //Ustawienia dot. danych, jakie chcemy pobierać z wiadomości

            imap.Behavior.RequestedHeaders = new string[] { MessageHeader.MessageId, MessageHeader.Bcc, MessageHeader.Cc, MessageHeader.Date, MessageHeader.From, MessageHeader.Subject, MessageHeader.To, MessageHeader.References, MessageHeader.InReplyTo };

            //Wybór folderu i załączników wraz z nagłówkami wiadomości

            FolderCollection folders = imap.Folders;
            var messages = imap.Folders["INBOX"].Search("ALL", MessageFetchMode.Attachments | MessageFetchMode.Headers, 100);

            using (var db = new DatabaseContext())
            {

                try
                {

                    foreach (ImapX.Message m in messages)
                    {

                        var checkMail = db.mails.Where(a => a.messageID == m.MessageId).FirstOrDefault();

                        if(checkMail != null)
                        {
                            continue;
                        }
                                                                      
                        if (!string.IsNullOrEmpty(m.MessageId))
                        {
                            var singleMassage = new Mails();

                            if (m.Date == null)
                            {
                                singleMassage.date = Convert.ToDateTime(DateTime.Now);
                            }
                            else
                            {
                                singleMassage.date = Convert.ToDateTime(m.Date);
                            }
                            singleMassage.messageID = m.MessageId;
                            singleMassage.inReplayTo = m.InReplyTo;
                            singleMassage.subject = m.Subject;
                            singleMassage.from = Convert.ToString(m.From);
                            singleMassage.to = JoinAddresses(m.To);
                            singleMassage.cc = JoinAddresses(m.Cc);
                            singleMassage.bcc = JoinAddresses(m.Bcc);
                            singleMassage.text = Convert.ToString(m.Body.Text);
                            singleMassage.html = Convert.ToString(m.Body.Html);
                            singleMassage.messageGuid = Guid.NewGuid();

                            //Zapis do bazy

                            db.mails.Add(singleMassage);


                            dateFolder = singleMassage.date.ToString("yyyy-MM-dd hh:mm:ss");
                            messageFolder = singleMassage.messageGuid.ToString();
                            dateFolder = dateFolder.Substring(0, 10);

                            string folderName = Properties.Settings.Default.MailDirectory + "\\" + dateFolder + "\\" + messageFolder;
                            System.IO.Directory.CreateDirectory(folderName);
                            exportMailstoXML(singleMassage);



                            //Sprawdzenie czy wiadomość zawiera załączniki

                            if (m.Attachments.Length > 0)
                            {
                                singleMassage.haveAttachment = true;
                                singleMassage.Attachments = new Collection<Attachments>();
                                Attachment[] atts = m.Attachments;

                                for (int j = 0; j < atts.Length; j++)
                                {
                                    Attachments singleMessageAttachment = new Attachments();
                                    atts[j].Download();
                                    atts[j].Save(folderName, atts[j].FileName);

                                    singleMessageAttachment.name = atts[j].FileName;
                                    singleMassage.Attachments.Add(singleMessageAttachment);
                                }


                            }

                            db.SaveChanges();
                            //m.MoveTo(imap.Folders.Sent);


                        }
                        else
                        {
                            continue;
                        }
                    }
                }

                catch (Exception ex)
                {


                    SenderProcessService.CatchError(ex);

                }
            }
            imap.Disconnect();
        }


        /// <summary>
        /// Metoda zapisująca pobrane wiadomości do plików xml
        /// </summary>
        /// <param name="mailsToExport"></param>
        public static void exportMailstoXML(Mails mailsToExport)
        {

            //Obróbka Daty w celu wskazania docelowego folderu dla wiadomości

            string fileNameToWrite = "content.xml";
            string dateFolder = mailsToExport.date.ToString("yyyy-MM-dd hh:mm:ss");
            dateFolder = dateFolder.Substring(0, 10);
            string destinationToWrite = @Properties.Settings.Default.MailDirectory + "\\" + dateFolder + "\\" + mailsToExport.messageGuid + "\\" + fileNameToWrite;

            if (File.Exists(destinationToWrite))
            {
                return;
            }
            else
            {
                try
                {
                    XmlGenerateMessage xml = new XmlGenerateMessage
                    {
                       
                        MessageID = mailsToExport.messageID,
                        MessageGuid = mailsToExport.messageGuid.ToString(),
                        InReplayTo = mailsToExport.inReplayTo,
                        From = mailsToExport.from,
                        Date = mailsToExport.date.ToString(),
                        To = mailsToExport.to,
                        Cc = mailsToExport.cc,
                        Bcc = mailsToExport.bcc,
                        Subject = mailsToExport.subject,
                        Content = mailsToExport.text,
                        Html = mailsToExport.html

                    };

                    XmlSerializer serializer = new XmlSerializer(typeof(XmlGenerateMessage));
                    using (TextWriter writer = new StreamWriter(destinationToWrite))
                    {
                        serializer.Serialize(writer, xml);
                    }

                }
                catch (Exception ex)
                {

                    SenderProcessService.CatchError(ex);

                }
            }
        }

        /// <summary>
        /// Metoda odpowiedzialna za odczyt treści wiadomości zapisanych na dysku
        /// </summary>
        /// <param name="mailsToImport"></param>
        /// <returns></returns>
        public List<Mails> importMailsFromXML(List<Mails> mailsToImport)
        {
            List<Mails> mailsFromDisk = new List<Mails>();

            try
            {

                foreach (var i in mailsToImport)
                {
                    //Obróbka daty w celu wskazania folderu źródłowego

                    string fileNameToRead = "content.xml";
                    string folderDate = i.date.ToString();
                    folderDate = folderDate.Substring(0, 10);

                    string source = Properties.Settings.Default.MailDirectory + "\\" + folderDate + "\\" + i.messageGuid + "\\" + fileNameToRead;

                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(source);
                    XmlNodeList xnList = xmlDoc.SelectNodes("/XmlGenerateMessage");

                    //Iteracja po węzłach pliku xml i zapis na listę

                    foreach (XmlNode xn in xnList)
                    {
                        var singleXMLmassage = new Mails();
                        singleXMLmassage.messageID = xn.ChildNodes[0].InnerText;
                        singleXMLmassage.messageGuid = i.messageGuid;
                        singleXMLmassage.inReplayTo = xn.ChildNodes[1].InnerText;
                        singleXMLmassage.from = xn.ChildNodes[3].InnerText;
                        singleXMLmassage.date = Convert.ToDateTime(xn.ChildNodes[4].InnerText);
                        singleXMLmassage.to = xn.ChildNodes[5].InnerText;
                        singleXMLmassage.cc = xn.ChildNodes[6].InnerText;
                        singleXMLmassage.bcc = xn.ChildNodes[7].InnerText;
                        singleXMLmassage.subject = xn.ChildNodes[8].InnerText;
                        singleXMLmassage.text = xn.ChildNodes[9].InnerText;
                        singleXMLmassage.html = xn.ChildNodes[10].InnerText;


                        mailsFromDisk.Add(singleXMLmassage);
                    }


                }


            }
            catch (Exception ex)
            {

                SenderProcessService.CatchError(ex);
            }
            return mailsFromDisk;
        }

        private static string JoinAddresses(List<ImapX.MailAddress> mailboxes)
        {
            return string.Join(",",
               new List<MailAddress>(mailboxes).ConvertAll(m => string.Format("{0}", m.Address))
                 .ToArray());
        }

        public static string clearMessageId(string messageID)
        {

            string clearedID = messageID;

            if (clearedID.Substring(0, 1) == "<")
            {
                clearedID = clearedID.Remove(0, 1);
            }
            if (clearedID.Substring(clearedID.Length - 1, 1) == ">")
            {
                clearedID = clearedID.Remove((clearedID.Length - 1));
            }

            return clearedID;
        }

        public MailPagedList<Mails> ListofMails(string mail, int pageNumber, int pageSize)
        {
            using (var db = new DatabaseContext())
            {

                var result = new MailPagedList<Mails>();
                var mailList = db.mails.AsQueryable();
                mailList = mailList.Where(a => a.to == mail).Include(a => a.Attachments);
                mailList = mailList.Where(a => a.isSendMessage != true);
                result.TotalRecords = mailList.Count();
                mailList = mailList.OrderByDescending(a => a.date);

                result.ResultList = mailList.Skip(pageNumber * pageSize).Take(pageSize).ToList();              
                  
                return result;
            }

        }

        public int ListofUnread(string mailAddress)
        {
            using (var db = new DatabaseContext())
            {
               
                var mt = db.mails.Where(a => a.to == mailAddress).Where(a => a.isRead == false).Count();

                int result = mt;
                return result;
            }

        }


        public Mails ShowMail(Guid guid)
        {

            using (var db = new DatabaseContext())
            {
                var mailMessage = db.mails.Where(a => a.messageGuid == guid).FirstOrDefault();
                mailMessage.isRead = true;
                db.SaveChanges();
                
                return mailMessage;
            }

        }

        public string GetMessageContent(DateTime date, Guid guid)
        {
            string fileNameToRead = "content.xml";
            string folderDate = date.ToString();
            folderDate = folderDate.Substring(0, 10);
            string MessageContent = "";

            string source = Properties.Settings.Default.MailDirectory + "\\" + folderDate + "\\" + guid + "\\" + fileNameToRead;

            try
            {

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(source);

                MessageContent = xmlDoc.DocumentElement.SelectSingleNode("/XmlGenerateMessage/Html").InnerText;
                if (string.IsNullOrEmpty(MessageContent))
                {
                    MessageContent = xmlDoc.DocumentElement.SelectSingleNode("/XmlGenerateMessage/Content").InnerText;
                }

            }
            catch (Exception ex)
            {

                SenderProcessService.CatchError(ex);
            }

            return MessageContent;
        }


        public string GetMessageReviever(Guid guid, DateTime date)
        {

            string fileNameToRead = "content.xml";
            string folderDate = date.ToString();
            folderDate = folderDate.Substring(0, 10);
            string MessageContent = "";

            string source = Properties.Settings.Default.MailDirectory + "\\" + folderDate + "\\" + guid + "\\" + fileNameToRead;

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(source);

            MessageContent = xmlDoc.DocumentElement.SelectSingleNode("/MailMessage/Mail/Html").InnerText;
            if (string.IsNullOrEmpty(MessageContent))
            {
                MessageContent = xmlDoc.DocumentElement.SelectSingleNode("/MailMessage/Mail/Treść").InnerText;
            }

            return MessageContent;
        }

        public List<GetEmailsDTO> DisplayAttachments(Guid guid)
        {
            using (var db = new DatabaseContext())
            {

                var attachments = db.mails.Where(b => b.messageGuid == guid).FirstOrDefault();

                var date = attachments.date.ToString();
                
                date = date.Substring(0, 10);

                var messageGuid = attachments.messageGuid.ToString();
                List<GetEmailsDTO> attachmentNames = new List<GetEmailsDTO>();

                try
                {

                    foreach (var i in attachments.Attachments)
                    {
                        var singleAttachment = new GetEmailsDTO();
                        singleAttachment.attachmentName = i.name;
                        singleAttachment.attachmentDate = date;


                        attachmentNames.Add(singleAttachment);
                    }



                }
                catch (Exception ex)
                {

                    SenderProcessService.CatchError(ex);
                }
                return attachmentNames;
            }



        }

  

    }
}