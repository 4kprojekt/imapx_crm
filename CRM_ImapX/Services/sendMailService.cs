﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Net;
using CRM_ImapX.Models.DB;
using System.Collections.ObjectModel;
using CRM_ImapX.Services;
using System.IO;
using ImapX.Constants;
using ImapX.Collections;
using ImapX.Enums;
using System.Data.Entity;
using CRM_ImapX.Models;
using CRM_ImapX.Models.Api;

namespace CRM_ImapX.Services
{
    public class sendMailService
    {

        public void sendEmail(string from, string to, string subject, string content, string cc, string bcc, List<CRM_ImapX.Models.Api.FileDTO> uploadedFiles, Guid msgGuid)
        {
            using (var db = new DatabaseContext())
            {

                try
                {
                    string dateFolder = null;
                    string messageFolder = null;
                    
                    var Mail = new Mails();
                    var sentMessage = new MessageSends();
                    Mails mailToCopyFiles = new Mails();

                    Mail.SentMessages = new MessageSends();
                    Mail.Attachments = new Collection<Attachments>();
                    Mail.MailtoAddress = new Collection<MailtoAddress>();

                    //Inicjacja nagłówka Message-Id
                    var message = new MailMessage();
                    message.Headers.Add("Message-Id",
                             String.Format("<{0}@{1}>",
                             Guid.NewGuid().ToString(),
                            "4kprojekt.pl"));

                    //Inicjacja obiektu Mail danymi i zapis do bazy

                    Mail.messageID = message.Headers.GetValues("Message-Id").FirstOrDefault();
                    Mail.subject = subject;
                    Mail.from = from;
                    Mail.to = to;
                    Mail.cc = cc;
                    Mail.bcc = bcc;
                    Mail.text = content;
                    Mail.date = DateTime.Now;
                    Mail.messageGuid = Guid.NewGuid();
                    Mail.isSendMessage = true;

                    //Inicjacja obiektu sentMessage danymi i zapis na listę obiektu Mail
                    //   sentMessage.messageID = Mail.messageID;
                    sentMessage.isSend = false;
                    sentMessage.date = Mail.date;
                    Mail.SentMessages = sentMessage;

                    //Sprawdzenie czy istnieje rekord w bazie z adresem e-mail pobranym z formularza                              
                    Mail = checkIfToAddressExist(to, Mail, db);
                    Mail = checkIfCcAddressExist(cc, Mail, db);
                    Mail = checkIfBCcAddressExist(bcc, Mail, db);

                    db.mails.Add(Mail);
                    db.SaveChanges();

                    var mailToSend = db.mails.Where(a => a.messageID == Mail.messageID).FirstOrDefault();
                    mailToCopyFiles = db.mails.Where(a => a.messageGuid == msgGuid).FirstOrDefault();
                 
                    if (uploadedFiles != null && uploadedFiles.Any())
                    {
                        //Dodaje załączniki do wysyłanej wiadomośći
                        message = PrepareAttachmentsToSend(uploadedFiles, message, msgGuid);
                        //Zapis nazw załączników do bazy
                        Mail = SaveAttachmentsToDataBase(uploadedFiles, Mail);

                       // Mail.haveAttachment = true;
                        db.SaveChanges();
                    }

                    dateFolder = Mail.date.ToString();
                    messageFolder = Mail.messageGuid.ToString();
                    dateFolder = dateFolder.Substring(0, 10);

                    string folderName = Properties.Settings.Default.MailDirectory + "\\" + dateFolder + "\\" + messageFolder;
                    System.IO.Directory.CreateDirectory(folderName);

                    //Export wiadomośći na pliku XML na dysk serwera
                    MailService.exportMailstoXML(Mail);
                    
                    //Inicjacja pól to, cc, bcc
                    message = initializeMailAddresses(Mail, message);

                    message.From = new MailAddress(mailToSend.from);
                    message.Subject = mailToSend.subject;
                    message.Body = mailToSend.text;
                    message.IsBodyHtml = true;

                    SmtpClient mailer = new SmtpClient(Properties.Settings.Default.Server, 587);
                    mailer.UseDefaultCredentials = false;
                    mailer.Credentials = new NetworkCredential(CRM_ImapX.Properties.Settings.Default.Login, CRM_ImapX.Properties.Settings.Default.Password);
                    mailer.EnableSsl = false;

                    if (msgGuid.ToString() == "00000000-0000-0000-0000-000000000000")
                    {
                        DateTime d = DateTime.Now;
                        SaveAttachmentsToDisk(uploadedFiles, folderName, msgGuid, d);
                    }
                    else
                    {
                        SaveAttachmentsToDisk(uploadedFiles, folderName, msgGuid, mailToCopyFiles.date);
                    }
                    //Tu następuje wywysłka wiadomości
                    MailMessagePrepareMessageToSend(message, mailer, sentMessage);

                    //Zapis załączników na dysk serwera w odpowiednie miejsce
                    
                }
                catch (Exception ex)
                {

                    SenderProcessService.CatchError(ex);
                }

            }



        }

        public MailPagedList<Mails> ListofSentMails(string mail, int pageNumber, int pageSize)
        {
            using (var db = new DatabaseContext())
            {
                var result = new MailPagedList<Mails>();
                var mailList = db.mails.AsQueryable();
                mailList = mailList.Where(a => a.from == mail);
                mailList = mailList.Where(a => a.isSendMessage != false);
                result.TotalRecords = mailList.Count();
                mailList = mailList.OrderByDescending(a => a.date);

                result.ResultList = mailList.Skip(pageNumber * pageSize).Take(pageSize).ToList();

                return result;
            }

        }

        public static void SaveAttachmentsToDisk(List<FileDTO> uploadedFiles, string folderName, Guid msgGuid, DateTime? date)
        {
            var d = "";
            try
            {

                d = date.ToString();
                d = d.Substring(0, 10);

                int i = 0;

                foreach (var file in uploadedFiles)
                {

                    if (file.isReturnForward == false)
                    {

                        string fileName = Path.GetFileName(file.Name);
                        var path = Path.Combine(folderName, fileName);
                        File.WriteAllBytes(path, file.Content);
                        i++;
                    }
                    else
                    {

                        var source = Path.Combine(CRM_ImapX.Properties.Settings.Default.MailDirectory + "\\" + d +"\\" + msgGuid.ToString(), file.Name);
                        var destination = Path.Combine(folderName, file.Name);
                        File.Copy(source, destination);
                        i++;
                    }
                }
        
            }
            catch (Exception ex)
            {

                SenderProcessService.CatchError(ex);
            }
        }

        public static Mails SaveAttachmentsToDataBase(List<FileDTO> uploadedFiles, Mails m)
        {

            try
            {
               
                  foreach (var file in uploadedFiles)
                {
                    
                    Attachments singleMessageAttachment = new Attachments();
                    singleMessageAttachment.name = file.Name;
                    m.Attachments.Add(singleMessageAttachment);
                    m.haveAttachment = true;
                }
            }
            catch (Exception ex)
            {

                SenderProcessService.CatchError(ex);
            }
            return m;
        }

        public static MailMessage PrepareAttachmentsToSend(List<FileDTO> files, MailMessage message, Guid msgGuid)
        {

            try
            {
                string date = "";
                string guid = "";
                using (var db = new DatabaseContext())
                {
                    if (msgGuid.ToString() != "00000000-0000-0000-0000-000000000000")
                    {
                        var mailMessage = db.mails.Where(a => a.messageGuid == msgGuid).FirstOrDefault();
                        date = mailMessage.date.ToString();
                        date = date.Substring(0, 10);
                        guid = msgGuid.ToString();
                    }
               
                foreach (var file in files)
                {
                   if(file.isReturnForward == false)
                   {
                       message.Attachments.Add(new Attachment(new MemoryStream(file.Content), Path.GetFileName(file.Name)));
                   }
                   else
                   {
                       var path = CRM_ImapX.Properties.Settings.Default.MailDirectory + "\\" + date + "\\" + guid + "\\" + file.Name;
                       message.Attachments.Add(new Attachment(path));
                      
                   }
                    
                   
                }

                }

            }
            catch (Exception ex)
            {

                SenderProcessService.CatchError(ex);
            }
            return message;
        }

        public static void MailMessagePrepareMessageToSend(MailMessage message, SmtpClient mailer, MessageSends sentMessage)
        {

            try
            {
                Queue<MailMessage> mailQueue = new Queue<MailMessage>();
                mailQueue.Enqueue(message);

                if (mailQueue.Count > 0)
                {
                    MailMessage messageFromQueue = mailQueue.Dequeue();
                    mailer.Send(messageFromQueue);
                }

                if (mailQueue.Count == 0)
                {
                    using (var db = new DatabaseContext())
                    {
                        var result = db.messageSend.Where(a => a.id == sentMessage.id).FirstOrDefault();
                        result.isSend = true;
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {

                SenderProcessService.CatchError(ex);
            }
        }

        public static List<string> listOfReceivers(string r)
        {
            List<string> receivers = new List<string>();
            string[] receiversDelimiter = r.Split(';');

            foreach (var i in receiversDelimiter)
            {

                receivers.Add(i);
            }


            return receivers;
        }

        public static Mails checkIfToAddressExist(string to, Mails Mail, DatabaseContext db)
        {

            try
            {
                var mailAddressTO = db.mailAddresses.Where(a => a.mail == to).FirstOrDefault();

                if (mailAddressTO == null)
                {
                    //Jeśli nie to utworz obiekty klasy MailAddress i zainicjuj go danymi
                    mailAddressTO = new MailAddresses();

                    mailAddressTO.displayName = to;
                    mailAddressTO.mail = to;
                }
                //Utwórz obiekt klasy MailToAddress i przypisz mu typ
                var MessageToAddressTO = new MailtoAddress();
                MessageToAddressTO.type = "to";
                MessageToAddressTO.MailAddress = mailAddressTO;
                Mail.MailtoAddress.Add(MessageToAddressTO);

            }
            catch (Exception ex)
            {

                SenderProcessService.CatchError(ex);
            }

            return Mail;
        }


        public static Mails checkIfCcAddressExist(string cc, Mails Mail, DatabaseContext db)
        {
            try
            {

                if (string.IsNullOrEmpty(cc))
                {
                    return Mail;
                }

                var MailAddressCC = db.mailAddresses.Where(a => a.mail == cc).FirstOrDefault();

                if (MailAddressCC == null)
                {
                    //Jeśli nie to utworz obiekty klasy MailAddress i zainicjuj go danymi
                    MailAddressCC = new MailAddresses();

                    MailAddressCC.displayName = cc;
                    MailAddressCC.mail = cc;

                }
                //Utwórz obiekt klasy MailToAddress i przypisz mu typ
                var MessageToAddressCC = new MailtoAddress();
                MessageToAddressCC.type = "cc";
                MessageToAddressCC.MailAddress = MailAddressCC;
                Mail.MailtoAddress.Add(MessageToAddressCC);
            }
            catch (Exception ex)
            {

                SenderProcessService.CatchError(ex);
            }
            return Mail;
        }


        public static Mails checkIfBCcAddressExist(string bcc, Mails Mail, DatabaseContext db)
        {
            try
            {

                if (string.IsNullOrEmpty(bcc))
                {
                    return Mail;
                }
                var MailAddressBCC = db.mailAddresses.Where(a => a.mail == bcc).FirstOrDefault();

                if (MailAddressBCC == null)
                {
                    //Jeśli nie to utworz obiekty klasy MailAddress i zainicjuj go danymi
                    MailAddressBCC = new MailAddresses();

                    MailAddressBCC.displayName = bcc;
                    MailAddressBCC.mail = bcc;
                }
                //Utwórz obiekt klasy MailToAddress i przypisz mu typ
                var MessageToAddressBCC = new MailtoAddress();
                MessageToAddressBCC.type = "bcc";
                MessageToAddressBCC.MailAddress = MailAddressBCC;
                Mail.MailtoAddress.Add(MessageToAddressBCC);

            }
            catch (Exception ex)
            {

                SenderProcessService.CatchError(ex);
            }
            return Mail;
        }

        public static MailMessage initializeMailAddresses(Mails Mail, MailMessage message)
        {
            try
            {

                foreach (var t in Mail.MailtoAddress)
                {

                    if (t.type == "to" && !string.IsNullOrEmpty(t.MailAddress.mail))
                    {
                        message.To.Add(new MailAddress(t.MailAddress.mail));
                    }

                    if (t.type == "cc" && !string.IsNullOrEmpty(t.MailAddress.mail))
                    {
                        message.CC.Add(new MailAddress(t.MailAddress.mail));
                    }

                    if (t.type == "bcc" && !string.IsNullOrEmpty(t.MailAddress.mail))
                    {
                        message.Bcc.Add(new MailAddress(t.MailAddress.mail));
                    }
                }
            }
            catch (Exception ex)
            {

                SenderProcessService.CatchError(ex);
            }
            return message;
        }
    }
}