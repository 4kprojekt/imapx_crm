﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Net.Mail;
using System.Net;
using CRM_ImapX.Models.DB;
using CRM_ImapX.Services;
using System.Xml;
using System.Net.Mime;
using System.IO;


namespace CRM_ImapX.Services
{
    public class SenderProcessService
    {
        private void AddMailAddressToMessage(MailMessage message, string addressType, Mails mail)
        {

            try
            {

                foreach (var mailAddress in mail.MailtoAddress.Where(ma => ma.type == addressType).Select(ma => ma.MailAddress.mail))
                {
                    switch (addressType)
                    {
                        case "bcc": message.Bcc.Add(new MailAddress(mailAddress));
                            break;
                        case "to": message.To.Add(new MailAddress(mailAddress));
                            break;
                        case "cc": message.CC.Add(new MailAddress(mailAddress));
                            break;
                        default:
                            throw new Exception("Nieznany TYP!");

                    }
                }

            }
            catch (Exception ex)
            {

                SenderProcessService.CatchError(ex);
            }
        }

        private void AddContentToMessage(MailMessage message, string date, string id)
        {
            string fileNameToRead = "content.xml";
            string folderDate = date;

            folderDate = folderDate.Substring(0, 10);

            try
            {

                string source = Properties.Settings.Default.MailDirectory + "\\" + folderDate + "\\" + id + "\\" + fileNameToRead;

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(source);
                XmlNodeList xnList = xmlDoc.SelectNodes("/XmlGenerateMessage");

                foreach (XmlNode xn in xnList)
                {
                    message.Body = xn.ChildNodes[9].InnerText;
                }

            }
            catch (Exception ex)
            {

                SenderProcessService.CatchError(ex);
            }

        }

        private static MailMessage AddAttachemntsToMessage(MailMessage message, string date, Mails Mail)
        {
            string folderDate = date;

            try
            {

                folderDate = folderDate.Substring(0, 10);

                foreach (var att in Mail.Attachments)
                {
                    string source = Properties.Settings.Default.MailDirectory + "\\" + folderDate + "\\" + Mail.messageGuid + "\\" + att.name;
                    Attachment data = new Attachment(source, MediaTypeNames.Application.Octet);
                    message.Attachments.Add(data);
                }
            }
            catch (Exception ex)
            {

                SenderProcessService.CatchError(ex);
            }

            return message;

        }

        public void sendMailsFromQueue()
        {
            using (var db = new DatabaseContext())
            {
               
                try
                {
                    var notSentMessages = db.mails.Include(a => a.Attachments).Include(a => a.SentMessages).Include(a => a.MailtoAddress).Where(e => e.SentMessages.isSend == false).ToList();
                    Queue<MailMessage> mailQueue = new Queue<MailMessage>();

                    SmtpClient mailer = new SmtpClient(Properties.Settings.Default.Server, 587);
                    mailer.Credentials = new NetworkCredential(Properties.Settings.Default.Login, Properties.Settings.Default.Password);
                    mailer.EnableSsl = false;

                    foreach (var m in notSentMessages)
                    {

                        var message = new MailMessage();
                        message.Headers.Add("M-ID", m.id.ToString());
                        message.Headers.Add("Message-Id", String.Format(m.messageID));
                        message.From = new MailAddress(m.from);
                        message.Subject = m.subject;

                        AddMailAddressToMessage(message, "to", m);
                        AddMailAddressToMessage(message, "cc", m);
                        AddMailAddressToMessage(message, "bcc", m);

                        AddContentToMessage(message, m.date.ToString(), m.messageGuid.ToString());
                        message = AddAttachemntsToMessage(message, m.date.ToString(), m);

                        mailQueue.Enqueue(message);

                    }

                    for (var q = 0; q < mailQueue.Count(); q++)
                    {

                        MailMessage messageFromQueue = mailQueue.Dequeue();
                        var key = messageFromQueue.Headers.GetValues("M-ID").FirstOrDefault();

                        mailer.Send(messageFromQueue);
                        var messageFlag = db.mails.Include(a => a.SentMessages).Where(a => a.id.ToString() == key).FirstOrDefault();
                        messageFlag.SentMessages.isSend = true;
                        db.SaveChanges();

                    }


                }
                catch (Exception ex)
                {

                    SenderProcessService.CatchError(ex);
                }

            }

        }

        public static void CatchError(Exception ex)
        {
            string filePath = Properties.Settings.Default.LogFilePath;

            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                writer.WriteLine("Message :" + ex.Message + "<br/>" + Environment.NewLine + "StackTrace :" + ex.StackTrace +
                  Environment.NewLine + "Date :" + DateTime.Now.ToString());
                if (ex.InnerException != null)
                {
                    writer.WriteLine("InnerException Message :" + ex.InnerException.Message + "<br/>" + Environment.NewLine + "StackTrace :" + ex.InnerException.StackTrace + Environment.NewLine);             
                }
                writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
            }

        }

    }
}