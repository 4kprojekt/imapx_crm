﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Net;
using CRM_ImapX.Models.DB;
using System.Collections.ObjectModel;
using CRM_ImapX.Services;
using System.IO;
using ImapX.Constants;
using ImapX.Collections;
using ImapX.Enums;
using System.Data.Entity;
using CRM_ImapX.Models.Api;

namespace CRM_ImapX.Services
{
    public class ReplyForwardService
    {

        public Mails GetReplayTo(Guid guid)
        {

            using (var db = new DatabaseContext())
            {
                var mailMessage = db.mails.Include(a => a.Attachments).Where(a => a.messageGuid == guid).FirstOrDefault();
                
                return mailMessage;
            }


        }

      
        
      public List<FileDTO> ForwardReplayAttachments(Mails mail)
            {
                var attachmentList = new List<FileDTO>();  
                
                string date = mail.date.ToString();
                date = date.Substring(0, 10);
               
                foreach(var att in mail.Attachments)
                {
                   
                    var path = CRM_ImapX.Properties.Settings.Default.MailDirectory + "\\" + date + "\\"+ mail.messageGuid + "\\" + att.name;
                   // FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
                   // BinaryReader br = new BinaryReader(fs);
                   
                    attachmentList.Add(new FileDTO
                        {
                            
                            Name = att.name
                        });
                    }

                return attachmentList;
                }
    
            }

    }


