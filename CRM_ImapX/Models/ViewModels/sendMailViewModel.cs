﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CRM_ImapX.Models.ViewModels
{
    public class sendMailViewModel
    {

        [Display(Name = "Od")]
        public string From { get; set; }

        [Required]        
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Błędny e-mail")]
        [Display(Name = "Do")]
        public string To { get; set; }

        public string Cc { get; set; }

        public string Bcc { get; set; }

        [Display(Name = "Temat")]
        public string Subject { get; set; }

        [Required]
        [AllowHtml]
        [UIHint("tinymce_full_compressed")]
        [Display(Name = "Treść")]
        public string Content { get; set; }

        [Display(Name = "Załącz")]
        public HttpPostedFileBase[] Upload { get; set; }

        public Guid messageGuid { get; set; }

        [Display(Name = "Data")]
        public string Date { get; set; }
        
    }
}