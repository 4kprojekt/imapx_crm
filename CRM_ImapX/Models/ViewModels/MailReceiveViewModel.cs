﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRM_ImapX.Models.ViewModels
{
    public class MailReceiveViewModel
    {

        public string MessageID { get; set; }

        public Guid MessageGuid { get; set; }

        [Display(Name = "Od")]
        public string From { get; set; }

        [Display(Name = "Do")]
        public string To { get; set; }

        public string Cc { get; set; }

        public string Bcc { get; set; }

        [Display(Name = "Data")]
        public DateTime Date { get; set; }

        [Display(Name = "Temat")]
        public string Subject { get; set; }

        [Display(Name = "Treść")]
        public string Text { get; set; }

        [Display(Name = "Treść")]
        public string Html { get; set; }

        public List<string> paths { get; set; }

        public List<string> attachmentsNames { get; set; }

        public bool isRead { get; set; }

        public bool haveAttachments { get; set; }
    
    }
}