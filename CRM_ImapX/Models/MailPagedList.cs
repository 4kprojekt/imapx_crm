﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CRM_ImapX.Models
{
    public class MailPagedList<T>
    {

        /// <summary>
        /// Lista zwracanych elementów
        /// </summary>
        public List<T> ResultList { get; set; }

        /// <summary>
        /// od którego rekordu zaczyna sie wyswietlanie
        /// </summary>
        public int PageNumber { get; set; }


        /// <summary>
        /// ilość rekordów na stronie
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// Ilość wszystkich rekordów
        /// </summary>
        public int TotalRecords { get; set; }

        /// <summary>
        /// Ilość stron
        /// </summary>
        public int TotalPages { get; set; }

        public int TotalUnread { get; set; }
    
    }
}