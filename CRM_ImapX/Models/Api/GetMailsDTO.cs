﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM_ImapX.Models.Api
{
    public class GetMailsDTO
    {

        public string MessageID { get; set; }

        public Guid MessageGuid { get; set; }      
        public string From { get; set; }
 
        public string To { get; set; }

        public string Cc { get; set; }

        public string Bcc { get; set; }
     
        public DateTime Date { get; set; }
      
        public string Subject { get; set; }
     
        [AllowHtml]
        public string Text { get; set; }
        [AllowHtml]
        public string Html { get; set; }

        public List<string> paths { get; set; }

        public List<string> attachmentsNames { get; set; }

        public bool isRead { get; set; }

        public bool haveAttachments { get; set; }

        public string content { get; set; }

        public List<FileDTO> files { get; set; }
 

    
    }
}