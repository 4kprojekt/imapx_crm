﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_ImapX.Models.Api
{
    public class FileDTO
    {
        public Byte[] Content { get; set; }
        public string Name { get; set; }
        public bool isReturnForward { get; set; }
    }
}