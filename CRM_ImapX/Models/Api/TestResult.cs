﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_ImapX.Models.Api
{
    public class TestResult
    {
        public int ResultAnyNumber { get; set; }
        public string ResultAnyString { get; set; }
    }
}