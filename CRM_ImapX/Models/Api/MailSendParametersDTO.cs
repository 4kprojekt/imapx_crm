﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM_ImapX.Models.Api
{
    public class MailSendParametersDTO
    {
        public string login { get; set; }

        public string to { get; set; }
        public string subject { get; set; }
        public string content { get; set; }
        public string cc { get; set; }
        public string bcc { get; set; }
        public List<FileDTO> uploadedFiles { get; set; }
        public Guid msgGuid { get; set; }
    
    
    }
}