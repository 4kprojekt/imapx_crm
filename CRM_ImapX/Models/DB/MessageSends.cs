﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace CRM_ImapX.Models.DB
{
    public class MessageSends
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        // public string messageID { get; set; }
        public bool isSend { get; set; }
        public DateTime date { get; set; }
    
    }
}