﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace CRM_ImapX.Models.DB
{
    public class Mails
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        
        [MaxLength(255)]
        public string messageID { get; set; }

        [MaxLength(255)]
        public string inReplayTo { get; set; }

        public string subject { get; set; }

        [MaxLength(255)]
        public string from { get; set; }

        [MaxLength(255)]
        public string to { get; set; }

        [MaxLength(255)]
        public string cc { get; set; }

        [MaxLength(255)]
        public string bcc { get; set; }

        public string text { get; set; }

        public string html { get; set; }

        public DateTime date { get; set; }

        public Guid messageGuid { get; set; }

        public bool isSendMessage { get; set; }

        public virtual ICollection<Attachments> Attachments { get; set; }

        public virtual MessageSends SentMessages { get; set; }

        public virtual ICollection<MailtoAddress> MailtoAddress { get; set; }

        [DefaultValue(false)]
        public bool isRead { get; set; }

        [DefaultValue(false)]
        public bool haveAttachment { get; set; }
    
    
    }
}