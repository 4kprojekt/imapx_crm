﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace CRM_ImapX.Models.DB
{
    public class MailtoAddress
    {
        [Key, Column(Order = 0)]
        [ForeignKey("Mail")]
        public int MailID { get; set; }

        [Key, Column(Order = 1)]
        [ForeignKey("MailAddress")]
        public int MailAddressesID { get; set; }

        public virtual Mails Mail { get; set; }
        public virtual MailAddresses MailAddress { get; set; }

        [MaxLength(255)]
        public string type { get; set; }

    }


}