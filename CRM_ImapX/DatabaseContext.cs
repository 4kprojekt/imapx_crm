﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using CRM_ImapX.Models.DB;
//using CRM_ImapX.Migrations;

namespace CRM_ImapX
{
    public class DatabaseContext : DbContext
    {


        public DbSet<Mails> mails { get; set; }

        public DbSet<Attachments> attachments { get; set; }

        public DbSet<MessageSends> messageSend { get; set; }

        public DbSet<MailAddresses> mailAddresses { get; set; }

        public DbSet<MailtoAddress> MailtoAddress { get; set; }


        public DatabaseContext()
            : base("DefaultConnection")
        {
        }

        //     protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //     {
        //       modelBuilder.Entity<Mails>().
        //         HasMany(c => c.MailAddresses).
        //          WithMany(p => p.Mails).
        //          Map(
        //          m =>
        //         {
        //             m.MapLeftKey("Mail_Id");
        //             m.MapRightKey("MailAddress_Id");
        //             m.ToTable("MailtoAddress");
        //         });
        //   }


    }
}