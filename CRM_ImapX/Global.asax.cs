﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Threading;
using CRM_ImapX.Services;
using CRM_ImapX.Models.ViewModels;
using System.IO;
using System.Web.Http;

namespace CRM_ImapX
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Thread threadStartCheckEmails = new Thread(new ThreadStart(CheckEmails));
            threadStartCheckEmails.Name = "CheckingThread";
            threadStartCheckEmails.Start();

            Thread threadStartSendMails = new Thread(new ThreadStart(SendEmails));
            threadStartSendMails.Name = "CheckingThreadSendMails";
            threadStartSendMails.Start();

        }
        public void CheckEmails()
        {

            while (true)
            {
                try
                {
                    var service = new MailService();
                    List<MailReceiveViewModel> receivedMails = new List<MailReceiveViewModel>();

                      service.ReciveUnseenImap();
                     

                }
                catch (Exception ex)
                {
                    Thread.Sleep(1000 * 30);
                    string filePath = Properties.Settings.Default.LogFilePath;

                    using (StreamWriter writer = new StreamWriter(filePath, true))
                    {
                        writer.WriteLine("Message :" + ex.Message + "<br/>" + Environment.NewLine + "StackTrace :" + ex.StackTrace +
                         "" + Environment.NewLine + "Date :" + DateTime.Now.ToString());
                        writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
                    }
                }
                Thread.Sleep(1000 * 30);
            }


        }


        public void SendEmails()
        {
            while (true)
            {
                try
                {

                        var s = new SenderProcessService();
                        s.sendMailsFromQueue();


                }
                catch (Exception ex)
                {
                    Thread.Sleep(1000 * 30);
                    string filePath = Properties.Settings.Default.LogFilePath;

                    using (StreamWriter writer = new StreamWriter(filePath, true))
                    {
                        writer.WriteLine("Message :" + ex.Message + "<br/>" + Environment.NewLine + "StackTrace :" + ex.StackTrace +
                         "" + Environment.NewLine + "Date :" + DateTime.Now.ToString());
                        writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
                    }
                }
                Thread.Sleep(1000 * 30);
            }


        }


    }
}
