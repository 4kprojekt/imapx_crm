﻿using CRM_ImapX.Models.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CRM_ImapX.Models.DB;
using CRM_ImapX.Services;
using System.IO;
using System.Net.Http.Headers;
using CRM_ImapX.Services.DTO;
using System.Web;

namespace CRM_ImapX.Controllers
{
    public class MailApiController : ApiController
    {

        public TestResult Get(Guid id)
        {

            return new TestResult
            {
                ResultAnyNumber = 1,
                ResultAnyString = "dasdas"
            };
        }
        [Route("mailList/{mailAddress}/{pageNumber}/{pageSize}/{orderColumn?}/{desc?}")]
        [HttpGet]
        public MailPagedListDTO GetList(string mailAddress, int pageNumber, int pageSize, string orderColumn = "", bool desc = true)
        {
            var mailList = new List<GetMailsDTO>();
            var service = new MailService();
            var result = service.ListofMails(mailAddress, pageNumber, pageSize);
            var unread = service.ListofUnread(mailAddress);
            var receivedMails = new MailPagedListDTO();
            receivedMails.ResultList = new List<GetMailsDTO>();

            foreach (var i in result.ResultList)
            {
                var mailMessage = new GetMailsDTO();
                mailMessage.MessageID = i.messageID;
                mailMessage.MessageGuid = i.messageGuid;
                mailMessage.From = i.from;
                mailMessage.Date = i.date;
                mailMessage.To = i.to;
                mailMessage.Cc = i.cc;
                mailMessage.Bcc = i.bcc;
                mailMessage.Subject = i.subject;
                mailMessage.haveAttachments = i.haveAttachment;
                mailMessage.isRead = i.isRead;

                if (i.haveAttachment == true)
                {
                    mailMessage.attachmentsNames = new List<string>();
                    foreach (var a in i.Attachments)
                    {
                        mailMessage.attachmentsNames.Add(a.name);
                    }
                }
                
                receivedMails.ResultList.Add(mailMessage);
            }

            
            receivedMails.TotalRecords = result.TotalRecords;
            receivedMails.PageNumber = result.PageNumber;
            receivedMails.TotalUnread = unread;

            return receivedMails;
        }

        [Route("showMessage/{guid}/")]
        [HttpGet]
        public GetMailsDTO GetMessage(Guid guid)
        {
           

            var service = new MailService();
            var mailMessage = service.ShowMail(guid);
           
            var ListOfAttachments = service.DisplayAttachments(guid);
            var date = mailMessage.date;
            var messageContent = service.GetMessageContent(date, guid);

            var messageToShow = new GetMailsDTO();

            messageToShow.From = mailMessage.from;
            messageToShow.Subject = mailMessage.subject;
            messageToShow.MessageGuid = mailMessage.messageGuid;
            messageToShow.Date = mailMessage.date;
            messageToShow.To = mailMessage.to;
            messageToShow.Cc = mailMessage.cc;
            messageToShow.Text = messageContent;
            messageToShow.content = messageContent;
            messageToShow.paths = new List<string>();
            messageToShow.attachmentsNames = new List<string>();
            messageToShow.isRead = mailMessage.isRead;
        
            byte[] newArray = new byte[ListOfAttachments.Count()];

            foreach (var i in ListOfAttachments)
            {
                
                string singlePath = Properties.Settings.Default.MailDirectory + "\\" + i.attachmentDate + "\\" + guid;

                messageToShow.paths.Add(singlePath);
                messageToShow.attachmentsNames.Add(i.attachmentName);
               
               
            }

            


            return messageToShow;
        }


        [Route("downloadAttachment/{date:DateTime}/{guid}/{name}")]
        [HttpGet]
        public HttpResponseMessage ReturnAttachments(DateTime date, Guid guid, string name)
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            var dateString = date.ToShortDateString();
            
            var path = Properties.Settings.Default.MailDirectory + "\\" + dateString + "\\" + guid + "\\" + name;
                       
               response.Content = new StreamContent(new FileStream(path, FileMode.Open, FileAccess.Read));
               response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
               response.Content.Headers.ContentDisposition.FileName = name;
                      
            return response;
                                   
        }

        [Route("mailSentList/{mailAddress}/{pageNumber}/{pageSize}/{orderColumn?}/{desc?}")]
        [HttpGet]
        public MailPagedListDTO GetSentList(string mailAddress, int pageNumber, int pageSize, string orderColumn = "", bool desc = true)
        {
            var mailList = new MailPagedListDTO();
            mailList.ResultList = new List<GetMailsDTO>();
            var service = new sendMailService();
            var result = service.ListofSentMails(mailAddress, pageNumber, pageSize);

            foreach (var i in result.ResultList)
            {
                var mailMessage = new GetMailsDTO();
                mailMessage.From = i.from;
                mailMessage.MessageGuid = i.messageGuid;
                mailMessage.Date = i.date;
                mailMessage.To = i.to;
                mailMessage.Cc = i.cc;
                mailMessage.Bcc = i.bcc;
                mailMessage.Subject = i.subject;
                mailMessage.haveAttachments = i.haveAttachment;
                mailMessage.isRead = i.isRead;

                mailList.ResultList.Add(mailMessage);
            }

            mailList.TotalRecords = result.TotalRecords;

            return mailList;
        }


        [Route("ForwardTo/{guid}")]
        [HttpGet]
        public GetMailsDTO Forward(Guid guid)
        {

            var service = new ReplyForwardService();
            var mailService = new MailService();

            var result = service.GetReplayTo(guid);
            var attachments = service.ForwardReplayAttachments(result);
            var mailForwardDTO = new GetMailsDTO();
            mailForwardDTO.files = new List<FileDTO>();
            mailForwardDTO.files = attachments;

            mailForwardDTO.Date = result.date;
            DateTime enteredDate = mailForwardDTO.Date;
            var content = mailService.GetMessageContent(enteredDate, guid);

            mailForwardDTO.Html = result.date + " " + result.from + Environment.NewLine + content;
            mailForwardDTO.Subject = "Fwd:" + " " + result.subject;
            mailForwardDTO.MessageGuid = result.messageGuid;
            mailForwardDTO.Cc = result.cc;
            mailForwardDTO.Bcc = result.bcc;

            return mailForwardDTO;
        }

        [Route("ReplayTo/{guid}")]
        [HttpGet]
        public GetMailsDTO Replay(Guid guid)
        {

            var service = new ReplyForwardService();
            var mailService = new MailService();
            var result = service.GetReplayTo(guid);
            var attachments = service.ForwardReplayAttachments(result);

            var mailReplayDTO = new GetMailsDTO();

            mailReplayDTO.files = new List<FileDTO>();
            mailReplayDTO.files = attachments;

            mailReplayDTO.Date = result.date;
            DateTime enteredDate = mailReplayDTO.Date;
            var content = mailService.GetMessageContent(enteredDate, guid);

            if (result.from.Contains("<"))
            {

                var input = result.from.Substring(result.from.IndexOf("<") + 1);
                input = input.Substring(0, input.LastIndexOf(">"));
                mailReplayDTO.To = input;
            }
            else
            {
                mailReplayDTO.To = result.from;

            }



            mailReplayDTO.Html = result.date + " " + result.from + Environment.NewLine + content;
            mailReplayDTO.Subject = "Re:" + " " + result.subject;
            mailReplayDTO.MessageGuid = result.messageGuid;
            mailReplayDTO.Cc = result.cc;
            mailReplayDTO.Bcc = result.bcc;
            mailReplayDTO.From = result.from;


            return mailReplayDTO;
        }

      
        [Route("CreateMessage")]
        [HttpPost]
        public void CreateMessage(MailSendParametersDTO l) 
        {

            
            MailSendParametersDTO a = new MailSendParametersDTO();
            var service = new sendMailService();

            service.sendEmail(l.login, l.to, l.subject, l.content, l.cc, l.bcc, l.uploadedFiles, l.msgGuid);
          
        }
    }
}
