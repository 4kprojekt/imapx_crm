﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM_ImapX.Services;
using CRM_ImapX.Models.ViewModels;
using System.IO;

namespace CRM_ImapX.Controllers
{
    public class MailSendController : Controller
    {
        // GET: MailSend

        public ActionResult IndexSentMails(MailPagedListViewModel model)
        {
            if (model.PageSize == 0)
            {
                model.PageSize = 10;
            }
                        
            var service = new sendMailService();
            var service2 = new MailService();
            var result = service.ListofSentMails("michal.latka@4kprojekt.pl", model.PageNumber, model.PageSize);

            var SentMails = new MailPagedListViewModel();
            SentMails.ResultList = new List<MailReceiveViewModel>();
          //  var listofMails = service2.importMailsFromXML(result);

            foreach (var i in result.ResultList)
            {
                var sentMail = new MailReceiveViewModel();

                sentMail.MessageID = i.messageID;
                sentMail.MessageGuid = i.messageGuid;
                sentMail.Subject = i.subject;
                sentMail.From = i.from;
                sentMail.To = i.to;
                sentMail.Cc = i.cc;
                sentMail.Bcc = i.bcc;
                sentMail.Date = i.date;
                sentMail.Text = i.text;
                sentMail.Html = i.html;
                sentMail.haveAttachments = i.haveAttachment;
                sentMail.isRead = i.isRead;

                SentMails.ResultList.Add(sentMail);
            }
            SentMails.PageSize = model.PageSize;
            SentMails.TotalRecords = result.TotalRecords;

            return View(SentMails);
        }

        public ActionResult Index()
        {


            return View();
        }

        [HttpPost]
        public ActionResult sendMail(CRM_ImapX.Models.ViewModels.sendMailViewModel model)
        {
            var service = new sendMailService();
            var uploadedFiles = new List<CRM_ImapX.Models.Api.FileDTO>();
            foreach (var file in model.Upload)
            {
                using (var binaryReader = new BinaryReader(file.InputStream))
                {
                    uploadedFiles.Add(new CRM_ImapX.Models.Api.FileDTO
                    {
                        Content = binaryReader.ReadBytes(file.ContentLength),
                        Name = file.FileName
                    });
                }
            }


            service.sendEmail(Properties.Settings.Default.Login, model.To, model.Subject, model.Content, model.Cc, model.Bcc, uploadedFiles, model.messageGuid);

            return RedirectToAction("Index");
        }


        [HttpGet]

        public ActionResult ReplayTo(CRM_ImapX.Models.ViewModels.MailReceiveViewModel model)
        {

            var service = new ReplyForwardService();
            var mailService = new MailService();
            var result = service.GetReplayTo(model.MessageGuid);

            var mailReplayVM = new sendMailViewModel();
            mailReplayVM.Date = result.date.ToString();
            DateTime enteredDate = DateTime.Parse(mailReplayVM.Date);
            var content = mailService.GetMessageContent(enteredDate, model.MessageGuid);

            if (result.from.Contains("<"))
            {

                var input = result.from.Substring(result.from.IndexOf("<") + 1);
                input = input.Substring(0, input.LastIndexOf(">"));
                mailReplayVM.To = input;
            }
            else
            {
                mailReplayVM.To = result.from;

            }



            mailReplayVM.Content = result.date + " " + result.from + Environment.NewLine + content;
            mailReplayVM.Subject = "Re:" + " " + result.subject;
            mailReplayVM.messageGuid = result.messageGuid;
            mailReplayVM.Cc = result.cc;
            mailReplayVM.Bcc = result.bcc;


            return View(mailReplayVM);
        }

        [HttpGet]
        public ActionResult ForwardTo(CRM_ImapX.Models.ViewModels.MailReceiveViewModel model)
        {
            var service = new ReplyForwardService();
            var mailService = new MailService();
            var result = service.GetReplayTo(model.MessageGuid);

            var mailForwardVM = new sendMailViewModel();
            mailForwardVM.Date = result.date.ToString();
            DateTime enteredDate = DateTime.Parse(mailForwardVM.Date);
            var content = mailService.GetMessageContent(enteredDate, model.MessageGuid);



            mailForwardVM.Content = result.date + " " + result.from + Environment.NewLine + content;
            mailForwardVM.Subject = "Fwd:" + " " + result.subject;
            mailForwardVM.messageGuid = result.messageGuid;
            mailForwardVM.Cc = result.cc;
            mailForwardVM.Bcc = result.bcc;

            return View(mailForwardVM);
        }


    }
}