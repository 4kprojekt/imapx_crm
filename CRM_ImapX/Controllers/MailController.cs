﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM_ImapX.Services;
using CRM_ImapX.Models.ViewModels;
using System.IO;


namespace CRM_ImapX.Controllers
{
    public class MailController : Controller
    {
        // GET: Mail
        public ActionResult Index(MailPagedListViewModel model)
        {
            if (model.PageSize == 0)
            {
                model.PageSize = 10;
            }
           
            var service = new MailService();
            service.ReciveUnseenImap();

            var result = service.ListofMails("michal.latka@4kprojekt.pl", model.PageNumber, model.PageSize);
            var receivedMails = new MailPagedListViewModel();
            receivedMails.ResultList = new List<MailReceiveViewModel>();
          
            foreach (var i in result.ResultList)
            {
                var rMail = new MailReceiveViewModel();


                rMail.MessageID = i.messageID;
                rMail.MessageGuid = i.messageGuid;
                rMail.Subject = i.subject;
                rMail.From = i.from;
                rMail.To = i.to;
                rMail.Cc = i.cc;
                rMail.Bcc = i.bcc;
                rMail.Date = i.date;
                rMail.Text = i.text;
                rMail.Html = i.html;
                rMail.isRead = i.isRead;
                rMail.haveAttachments = i.haveAttachment;
               
                receivedMails.ResultList.Add(rMail);
                
                
            }

            receivedMails.PageSize = model.PageSize;
            receivedMails.TotalRecords = result.TotalRecords;
         
            return View(receivedMails);

            
        }


        public ActionResult Show(CRM_ImapX.Models.ViewModels.MailReceiveViewModel model)
        {

            var service = new MailService();
            var result = service.ShowMail(model.MessageGuid);
            var messageContent = service.GetMessageContent(result.date, model.MessageGuid);
            var ListOfAttachments = service.DisplayAttachments(model.MessageGuid);


            var mailMessageVM = new MailReceiveViewModel();
            mailMessageVM.From = result.from;
            mailMessageVM.Subject = result.subject;
            mailMessageVM.MessageGuid = result.messageGuid;
            mailMessageVM.To = result.to;
            mailMessageVM.Cc = result.cc;
            mailMessageVM.Text = messageContent;
            mailMessageVM.paths = new List<string>();
            mailMessageVM.attachmentsNames = new List<string>();
            foreach (var i in ListOfAttachments)
            {
                string singlePath = Properties.Settings.Default.MailDirectory + "\\" + i.attachmentDate + "\\" + model.MessageGuid;

                mailMessageVM.paths.Add(singlePath);
                mailMessageVM.attachmentsNames.Add(i.attachmentName);

            }

            return View(mailMessageVM);
        }
        public ActionResult Download(string path, string filename)
        {

            byte[] fileBytes = System.IO.File.ReadAllBytes(path + "\\" + filename);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, filename);
        }


    }
}