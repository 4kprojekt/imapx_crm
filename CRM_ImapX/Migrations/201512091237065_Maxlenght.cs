namespace CRM_ImapX.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Maxlenght : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MailAddresses", "mail", c => c.String(maxLength: 255));
            AlterColumn("dbo.Mails", "messageID", c => c.String(maxLength: 255));
            AlterColumn("dbo.Mails", "inReplayTo", c => c.String(maxLength: 255));
            AlterColumn("dbo.Mails", "from", c => c.String(maxLength: 255));
            AlterColumn("dbo.Mails", "to", c => c.String(maxLength: 255));
            AlterColumn("dbo.Mails", "cc", c => c.String(maxLength: 255));
            AlterColumn("dbo.Mails", "bcc", c => c.String(maxLength: 255));
            AlterColumn("dbo.MailtoAddresses", "type", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MailtoAddresses", "type", c => c.String());
            AlterColumn("dbo.Mails", "bcc", c => c.String());
            AlterColumn("dbo.Mails", "cc", c => c.String());
            AlterColumn("dbo.Mails", "to", c => c.String());
            AlterColumn("dbo.Mails", "from", c => c.String());
            AlterColumn("dbo.Mails", "inReplayTo", c => c.String());
            AlterColumn("dbo.Mails", "messageID", c => c.String());
            AlterColumn("dbo.MailAddresses", "mail", c => c.String());
        }
    }
}
