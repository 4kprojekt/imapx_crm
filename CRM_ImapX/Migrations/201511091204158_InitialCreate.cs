namespace CRM_ImapX.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Attachments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        Mails_id = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Mails", t => t.Mails_id)
                .Index(t => t.Mails_id);
            
            CreateTable(
                "dbo.MailAddresses",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        displayName = c.String(),
                        mail = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Mails",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        messageID = c.String(),
                        inReplayTo = c.String(),
                        subject = c.String(),
                        from = c.String(),
                        to = c.String(),
                        cc = c.String(),
                        bcc = c.String(),
                        text = c.String(),
                        html = c.String(),
                        date = c.DateTime(nullable: false),
                        messageGuid = c.Guid(nullable: false),
                        isSendMessage = c.Boolean(nullable: false),
                        SentMessages_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.MessageSends", t => t.SentMessages_id)
                .Index(t => t.SentMessages_id);
            
            CreateTable(
                "dbo.MailtoAddresses",
                c => new
                    {
                        MailID = c.Int(nullable: false),
                        MailAddressesID = c.Int(nullable: false),
                        type = c.String(),
                    })
                .PrimaryKey(t => new { t.MailID, t.MailAddressesID })
                .ForeignKey("dbo.Mails", t => t.MailID, cascadeDelete: true)
                .ForeignKey("dbo.MailAddresses", t => t.MailAddressesID, cascadeDelete: true)
                .Index(t => t.MailID)
                .Index(t => t.MailAddressesID);
            
            CreateTable(
                "dbo.MessageSends",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        isSend = c.Boolean(nullable: false),
                        date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Mails", "SentMessages_id", "dbo.MessageSends");
            DropForeignKey("dbo.MailtoAddresses", "MailAddressesID", "dbo.MailAddresses");
            DropForeignKey("dbo.MailtoAddresses", "MailID", "dbo.Mails");
            DropForeignKey("dbo.Attachments", "Mails_id", "dbo.Mails");
            DropIndex("dbo.MailtoAddresses", new[] { "MailAddressesID" });
            DropIndex("dbo.MailtoAddresses", new[] { "MailID" });
            DropIndex("dbo.Mails", new[] { "SentMessages_id" });
            DropIndex("dbo.Attachments", new[] { "Mails_id" });
            DropTable("dbo.MessageSends");
            DropTable("dbo.MailtoAddresses");
            DropTable("dbo.Mails");
            DropTable("dbo.MailAddresses");
            DropTable("dbo.Attachments");
        }
    }
}
