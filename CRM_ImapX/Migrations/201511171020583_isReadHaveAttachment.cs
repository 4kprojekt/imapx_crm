namespace CRM_ImapX.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class isReadHaveAttachment : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Mails", "isRead", c => c.Boolean(nullable: false));
            AddColumn("dbo.Mails", "haveAttachment", c => c.Boolean(nullable: false));
        }
        /// <summary>
        /// 
        /// </summary>
        public override void Down()
        {
            DropColumn("dbo.Mails", "haveAttachment");
            DropColumn("dbo.Mails", "isRead");
        }
    }
}
