﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CRM_ImapX.Startup))]
namespace CRM_ImapX
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
